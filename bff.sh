#!/usr/bin/env bash

set -Eeuo pipefail
# set -x # debugging

iosevka_ver="5.0.0"
nerd_fonts_ver="2.1.0"
# all_the_icons_ver="4.0.1"
julia_mono_ver="0.032"

fetch() {
  wget -q --show-progress "${1}" -O - | tar xzf -
}

patch_font() {
  ./font-patcher -l -c --custom "${1}" --careful --progressbars -out "../../ttf" "${2}"
}

iosevka_dir="Iosevka-${iosevka_ver}"
nerd_fonts_dir="nerd-fonts-${nerd_fonts_ver}"
julia_mono_dir="JuliaMono-${julia_mono_ver}"

# Make working directories
mkdir -p cache work

pushd cache

# Fetch the files
if [ -d "${iosevka_dir}" ]; then
  printf "Found existing %s, skipping download\n" "${iosevka_dir}"
else
  printf "Downloading Iosevka\n"
  fetch "https://github.com/be5invis/Iosevka/archive/v${iosevka_ver}.tar.gz"
fi
printf "\n"

if [ -d "${nerd_fonts_dir}" ]; then
  printf "Found existing %s, skipping download\n" "${nerd_fonts_dir}"
else
  printf "Downloading Nerd Fonts\n"
  fetch "https://github.com/ryanoasis/nerd-fonts/archive/v${nerd_fonts_ver}.tar.gz"
fi
printf "\n"

if [ -d "${julia_mono_dir}" ]; then
  printf "Found existing %s, skipping download\n" "${julia_mono_dir}"
else
  printf "Downloading Julia Mono\n"
  mkdir -p ${julia_mono_dir}
  wget -q --show-progress "https://github.com/cormullion/juliamono/releases/download/v${julia_mono_ver}/JuliaMono-ttf.tar.gz" -O - | tar xzf - -C "${julia_mono_dir}/"
fi
printf "\n"

popd # root
pushd work

# Build Iosevka
printf "Building Iosevka\n"
cp -r "../cache/${iosevka_dir}" .
cp "../private-build-plans.toml" ${iosevka_dir}
pushd ${iosevka_dir}
npm install >/dev/null
npm run build -- ttf::bff
popd # work
printf "\n"

# Patch Nerd Font's font patcher
# patch allows font name to stay unchanged when patched (with -w flag)
printf "Patching Nerd-Font's font patcher\n"
cp -r "../cache/$nerd_fonts_dir" .
pushd "${nerd_fonts_dir}"

patch -p1 --forward <"../../font-patcher.patch"
printf "Done!\n\n"

# Copy glyphs
printf "Patching fonts…\n"
cp ../../cache/${julia_mono_dir}/*.ttf src/glyphs

# Patch fonts
julia_weights=("Light" "Regular" "Medium" "Bold" "ExtraBold" "Black")
iosevka_styles=("light"
  "regular"
  "medium"
  "bold"
  "extrabold"
  "heavy"
  "lightoblique"
  "oblique"
  "mediumoblique"
  "boldoblique"
  "extraboldoblique"
  "heavyoblique"
  "lightitalic"
  "italic"
  "mediumitalic"
  "bolditalic"
  "extrabolditalic"
  "heavyitalic")
tmpdir=$(mktemp -d)

printf "Logging to ${tmpdir}\n"
parallel --eta --bar --files --res "${tmpdir}" \
  ./font-patcher -l -c --careful --quiet -out "../../ttf" \
  --custom "JuliaMono-{2}.ttf" "../${iosevka_dir}/dist/bff/ttf/bff-{1}.ttf" \
  ::: "${iosevka_styles[@]}" \
  :::+ "${julia_weights[@]}" "${julia_weights[@]}" "${julia_weights[@]}" \
  >/dev/null

popd # work
popd # root

rm -rf "work"

printf 'All done! Look in ./ttf! :)\n'
